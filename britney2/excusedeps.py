from typing import TYPE_CHECKING, Optional, Union

from britney2.policies import PolicyVerdict

if TYPE_CHECKING:
    from . import DependencyType, PackageId


class DependencySpec(object):
    def __init__(
        self, deptype: "DependencyType", architecture: Optional[str] = None
    ) -> None:
        self.deptype = deptype
        self.architecture = architecture
        assert self.architecture != "all", "all not allowed for DependencySpec"


class DependencyState(object):
    dep: Optional[Union[str, "PackageId"]]

    def __init__(self, dep: Union[str, "PackageId"]) -> None:
        """State of a dependency

        :param dep: the excuse that we are depending on

        """
        self.valid = True
        self.verdict = PolicyVerdict.PASS
        self.dep = dep

    @property
    def possible(self) -> bool:
        return True

    def invalidate(self, verdict: PolicyVerdict) -> None:
        self.valid = False
        self.verdict = PolicyVerdict.worst_of(verdict)


class ImpossibleDependencyState(DependencyState):
    """Object tracking an impossible dependency"""

    def __init__(self, verdict: PolicyVerdict, desc: str) -> None:
        """

        :param desc: description of the impossible dependency

        """
        self.valid = False
        self.verdict = verdict
        self.desc = desc
        self.dep = None

    @property
    def possible(self) -> bool:
        return False
