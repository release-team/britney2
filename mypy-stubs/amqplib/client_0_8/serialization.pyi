from typing import Any

class GenericContent:
    def __init__(self, **props: Any) -> None: ...
