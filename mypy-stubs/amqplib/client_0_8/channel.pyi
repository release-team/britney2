from .abstract_channel import AbstractChannel
from .basic_message import Message
from typing import Any

__all__ = ["Channel"]

class Channel(AbstractChannel):
    def __init__(
        self, connection: Any, channel_id: int | None = None, auto_decode: bool = True
    ) -> None: ...
    def basic_publish(
        self,
        msg: Message,
        routing_key: str = "",
    ) -> None: ...
