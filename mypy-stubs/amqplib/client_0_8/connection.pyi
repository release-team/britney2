from .abstract_channel import AbstractChannel
from .channel import Channel

__all__ = ["Connection"]

class Connection(AbstractChannel):
    def __init__(
        self, host: str = "localhost", userid: str = "guest", password: str = "guest"
    ) -> None: ...
    def channel(self, channel_id: int | None = None) -> Channel: ...
    def close(self) -> None: ...
