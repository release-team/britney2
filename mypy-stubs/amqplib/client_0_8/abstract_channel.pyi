from _typeshed import Incomplete
from collections.abc import Callable
from typing import Any
import types
from contextlib import AbstractContextManager

__all__ = ["AbstractChannel"]

class AbstractChannel(AbstractContextManager["AbstractChannel"]):
    connection: Incomplete
    channel_id: Incomplete
    method_queue: Incomplete
    auto_decode: bool
    _METHOD_MAP = dict[tuple[int, int], Callable[[Any], Any]]
    def __init__(self, connection: Any, channel_id: int) -> None: ...
    def __enter__(self) -> "AbstractChannel": ...
    def __exit__(
        self,
        type: type[BaseException] | None,
        value: BaseException | None,
        traceback: types.TracebackType | None,
    ) -> None: ...
    def close(self) -> None: ...
    def wait(self, allowed_methods: Any | None = None) -> Any: ...
    def dispatch_method(
        self, method_sig: tuple[int, int], args: Any, content: Any | None
    ) -> Callable[[Any], Any]: ...
