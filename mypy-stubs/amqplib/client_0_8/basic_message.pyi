from typing import Any
from .serialization import GenericContent

__all__ = ["Message"]

class Message(GenericContent):
    def __init__(self, body: str = "", **properties: Any) -> None: ...
